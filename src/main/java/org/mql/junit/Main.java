package org.mql.junit;

public class Main {
    public static void main(String[] args) {
        if(args!=null && args.length > 1){
            String word = args[0];
            String lang = args[1];
            System.out.println(new Translator(lang).translate(word));
        }else{
            System.err.println("Two arguments should be provided to translate correctly");
            System.exit(1);
        }
    }
}
