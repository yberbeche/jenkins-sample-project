package org.mql.junit;

import org.junit.*;
import static org.junit.Assert.assertEquals;

public class TranslatorTest {

    @Test
    public void translateHelloToFrench(){
        System.out.println(">> translateHelloToFrench");
        String result = new Translator("fr").translate("hello");
        assertEquals("salut", result);
    }

    @Test
    public void translateMorningToFrench(){
        System.out.println(">> translateMorningToFrench");
        String result = new Translator("fr").translate("morning");
        assertEquals("matin", result);
    }

    @Test
    public void translateNightToFrench(){
        System.out.println(">> translateNightToFrench");
        String result = new Translator("fr").translate("night");
        assertEquals("nuit", result);
    }

    @Test
    public void translateHelloToSpanish(){
        System.out.println(">> translateHelloToSpanish");
        String result = new Translator("es").translate("hello");
        assertEquals("hola", result);
    }

    @Test
    public void translateMorningToSpanish(){
        System.out.println(">> translateMorningToSpanish");
        String result = new Translator("es").translate("morning");
        assertEquals("mañana", result);
    }

    @Test
    public void translateNightToSpanish(){
        System.out.println(">> translateNightToSpanish");
        String result = new Translator("es").translate("night");
        assertEquals("noche", result);
    }
}
